#!/usr/bin/env python3
#used by update_p13_data.sc!!!

# import the needed packages
backend = 'Agg'

import os, sys,numpy,datetime
import csv

#######################################

def dayshift(dtgb,fp):
     TP= datetime.date(int(dtgb[0:4]),int(dtgb[4:6]),int(dtgb[6:8]))
     TP2 = TP + datetime.timedelta(days=fp)
     dtg= datetime.datetime.strftime(TP2,'%Y%m%d')
     return str(dtg)

######################################################################
def wrmerg(filexp,station,params,datums,values,nval):
     csvf=open(filexp,'w+')
     nnan = 0
     print ( station,' wrmerg params',params)
#     for jpar in range(3):
     csvf.write("%s" % 'dtg')
     csvf.write("%s" %  ',')
     csvf.write("%s" % 'ststion')
     csvf.write("%s" %  ',')
     csvf.write("%s" % params[0].strip())
     csvf.write('\n')
     nextdat = datums[0]
     for jdat in range(len(datums)):
          datum=datums[jdat]
          if datum != nextdat: print(' gapp',datum,nextdat)
          csvf.write("%s" %  datum)
          csvf.write("%s" %  ',')
          csvf.write("%s" %  station)
          csvf.write("%s" %  ',')
          if numpy.isnan(values[params[0],datums[jdat]]) :
                    nnan +=1
                    print ('alarm: missing',datum,station,values[params[0],datums[jdat]])
          else:
                     val = values[params[0],datums[jdat]]/nval[params[0],datums[jdat]]
                     csvf.write("%s" % val)
          csvf.write('\n')
          nextdat = dayshift(datum,1)
     csvf.close()
     return filexp

######################################################################
def read_dtb(filedtb):
    values={}
    nval={}
    nfound,nmiss = 0,0 
    mparam = []
    datlist = []
    lnr=0
    if not os.path.exists(filedtb):
          print (' no filedtb : ',filedtb)
          sys.exit()
    with open(filedtb) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
          
          if lnr == 0:
              for jpar in range (len(row)):
                     cont = row[jpar].split('.')
                     try:
                          param = cont[1]
                     except:
                          param = cont[0]
                     if (jpar > 1) :
                           if len(param) > 0:
                                mparam.append(param)
              print (' found :',mparam)
               
          else:
                if len(row) < 3 : continue
                station=row[1]
                datetime=row[0]
                datum=datetime[0:8]
#                check = datum[0:6] 
#                if check == '200210' :  print (row)
                if row[2] == '' : continue
                for jpar,param in enumerate(mparam):
                     try:
                        val=float(row[jpar+2])
                        values[param,datum]=val
                        nval[param,datum]=1
                        datlist.append(datum)
                     except:
                        val=numpy.nan
                        nmiss += 1
                nfound +=1
          lnr+=1
        if nfound == 0 :
             print (' no data found', filedtb)
             sys.exit()
    print (' read ',nfound,'missed',nmiss,len(datlist),len(values),len(nval))

    return datlist, values,nval,station,mparam

######################################################################

def  match_params(param1,param2):

     parammatch=[]
     for jp1 in range(len(param1)):
          for jp2 in range(len(param2)):
               if param1[jp1] == param2[jp2] : 
                         parammatch.append(param1[jp1])
     return parammatch
######################################################################

def wrdefdat(filexp,station,params,datums,values,nval):
     csvf=open(filexp,'w+')
     header = 'NL gemiddeld'
     csvf.write("%s" % header)
     csvf.write('\n')
     nnan = 0
     csvf.write("%s" % 'day')
     csvf.write("%s" % ',')
     csvf.write("%s" % params[0])
     csvf.write('\n')

     jpar=0
     for jdat in range(len(datums)):
          datum=datums[jdat]
          datum = datum[0:4] + '-'   + datum [4:6] + '-' + datum[6:8]
          csvf.write("%s" %  datum)
          csvf.write("%s"  % ',')
          try:
              if numpy.isnan(values[params[jpar],datums[jdat]]) :
                    csvf.write("%s"  % ' ')
                    print ('write missing',datum)
                    nnan +=1
              else:
                     val = values[params[jpar],datums[jdat]]/nval[params[jpar],datums[jdat]]
                     csvf.write("%7.2f" % val)
              csvf.write('\n')
          except:
                 print ( 'missed',datums[jdat])
     csvf.close()
     return filexp


###############################################################
def ordat(datumsold):
     dats=numpy.sort(datumsold)
     datums=[]
     datums.append(dats[0])
     for jdat in range(len(dats)):
         if int(dats[jdat]) >  int(datums[-1]) : datums.append(dats[jdat])
     return datums
######################################################################

def Addup(datums1,values1,nval1,datums2,values2,nval2,params):
     print ( 'addup',len(values1),len(values2),len(nval1),len(nval2))
     repl,upd=0,0
     odnan = 0
     nkey = 0
     nnan = 0
     values=values1.copy()
     nval=nval1.copy()
     datums=datums1.copy()
     for datum in datums2:
          datums.append(datum)
          for param in params:
             if numpy.isnan(values2[param,datum])  :
                 try:
                      if not numpy.isnan(values[param,datum]):  odnan+=1
                 except:
                      nnan+=1
             else:
               upd +=1
               try:
                 values[param,datum]=values1[param,datum]+values2[param,datum]
                 nval[param,datum]=nval1[param,datum]+nval2[param,datum]
               except:
                 nkey+=1
                 values[param,datum]=values2[param,datum]
                 nval[param,datum]=nval2[param,datum]

     print ('merged ',upd,'odd nan ',odnan, 'none fix',nnan, 'new key ',nkey)
     print ( 'len datums',len(datums))
     return datums,values,nval
######################################################################


### main
print('-----------------------------------------------------------')
print (' Average_one.py  calculate arverage of  parameters from several files to one')
print (' Update 30-4-2021: accepts unequal dtg list input ')
if len(sys.argv) < 3:     
     print (' start : Average_one csvfile csvfile csvfile')
else:

     stat=[]
     filedtb = sys.argv[1]

     npos = filedtb.find('/')
     if npos > 0 :
          direc = filedtb[0:npos]
     else:
          direc = '.'
     basenm=filedtb[npos+1:]
     datums,values,nval,station,param = read_dtb(filedtb)
     stat.append(station)
     print ( 'read', filedtb,station,param)
     nstat=1
     basenm=basenm.replace(station, 'NL')
     basenm=basenm.replace('.csv', '')
     flnr = 2
     while len(sys.argv) > flnr:
          filetoadd = sys.argv[flnr]
          datums2,values2,nval2,station2,param2 = read_dtb(filetoadd)
          stat.append(station2)
          print ( 'read', filetoadd,station2,param2)
          print ( ' Addup',station,station2)
          params=match_params(param,param2)
          print (' matched',params)
          datums,values,nval=Addup(datums,values,nval,datums2,values2,nval2,params)
          
          nstat += 1
          flnr += 1
     if True : 
          filexp=direc+'/'+basenm+'_daily.csv'
          filP13=direc+'/'+basenm+'.csv'
     print ( 'len check',len(params),len(datums),len(values))
     print (len(params),"params:",params)
     sel='NL'
     print ( 'len shiot',len(datums))
     datums=ordat(datums)
     print ( 'len ordered',len(datums))
     OK=wrdefdat(filexp,sel,params,datums,values,nval)
     OK=wrmerg(filP13,sel,params,datums,values,nval)
     print (filexp)
     print (filP13)

print('######################################################################')

