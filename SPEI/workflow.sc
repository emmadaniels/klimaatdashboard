#!/bin/csh
#workflow for SPEI calculations Klimaatdashboard
#will place retrieved and output data in ./DATA

csh update_p13_data.sc
Rscript SPEI_P13.R
Rscript runtrend.R ./DATA/SPEI_year.csv 'drawplot="SPEI [-]"'
Rscript runtrend.R ./DATA/SPEI_DJF.csv 'drawplot="SPEI [-] winter"'
Rscript runtrend.R ./DATA/SPEI_MAM.csv 'drawplot="SPEI [-] lente"'
Rscript runtrend.R ./DATA/SPEI_JJA.csv 'drawplot="SPEI [-] zomer"'
Rscript runtrend.R ./DATA/SPEI_SON.csv 'drawplot="SPEI [-] herfst"'
