#!/usr/bin/env python3
#used by update_p13_data.sc!!!

# import the needed packages
backend = 'Agg'

import os, sys,numpy,datetime
import csv


######################################################################
def read_dtb(filedtb):
    values={}
    nfound = 0 
    mparam = []
    datlist = []
    lnr=0
    if not os.path.exists(filedtb):
          print (' no filedtb : ',filedtb)
          sys.exit()
    with open(filedtb) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
          
          if lnr == 0:
              for jpar in range (len(row)):
                     cont = row[jpar].split('.')
                     try:
                          param = cont[1]
                     except:
                          param = cont[0]
                     mparam.append(param)
              print (' found :',mparam)
               
          else:
                station=row[1]
                datetime=row[0]
                datum=datetime[0:8]
                for jpar,param in enumerate(mparam):
                     try:
#                     if True:
                        val=float(row[jpar+2])
                        if station == '550_H' :
                            if int(datum) < 19940100 :
                                  val = val / 1.231
                     except:
                        val=numpy.nan
                        
                     values[param,datum]=val
                     nfound +=1
                datlist.append(datum)
          lnr+=1
        if nfound == 0 :
             print (' no data found',dtgbeg,dtgend)
             sys.exit()
        if station == '550_H' : print (' Benschop correction ',station)

    return datlist, values,station,mparam

######################################################################

def  match_params(param1,param2):

     parammatch=[]
     for jp1 in range(len(param1)):
          for jp2 in range(len(param2)):
               if param1[jp1] == param2[jp2] : 
                         parammatch.append(param1[jp1])
     return parammatch
######################################################################

def wrmerg(filexp,station,params,datums,values):
     csvf=open(filexp,'w+')
     nnan = 0
     for jpar in range(len(params)):
           csvf.write("%s" % params[jpar].strip())
           csvf.write("%s" % ',')
     csvf.write('\n')
     for jdat in range(len(datums)):
          datum=datums[jdat]
          csvf.write("%s" %  datums[jdat])
          csvf.write("%s" %  ',')
          csvf.write("%s" %  station)
          for jpar in range(len(params)):
                csvf.write("%s"  % ',')
                param=params[jpar]
                if numpy.isnan(values[params[jpar],datums[jdat]]) :
                    nnan +=1
                else:
                     csvf.write("%s" % values[params[jpar],datums[jdat]])
          csvf.write('\n')
     csvf.close()
     return filexp

######################################################################

def merge(datums1,values1,datums2,values2,params):
     repl,upd=0,0
     values=values1.copy()
     datums = datums1.copy()
     print ( len (values))
     for datum in datums2:
          for param in params:
                 upd +=1
                 values[param,datum]=values2[param,datum]
                 if datum > datums[-1] :  datums.append(datum)
     return datums,values
######################################################################


### main
print('-----------------------------------------------------------')
print (' Merge_several.py  copies parameters from several files to one, overwriting values of former by later  for same date')
if len(sys.argv) < 3:     
     print (' start : Merge_several csvfile csvfile csvfile')
else:

     logf='logfile.log'
     log=open(logf ,'w+')
     log.write("%s" % 'Logfile Merge_several ')
     log.write("\n")

     filedtb = sys.argv[1]

     datums,values,station,param = read_dtb(filedtb)
     print ( 'read', filedtb,station,param)
     log.write("%s" % ' Stations ')
     log.write("\n")
     log.write("%s " % 'first')
     log.write("%s " % station)
     log.write("%s " % ':')
     log.write("%s " % datums[0])
     log.write("%s " % 'till')
     log.write("%s " % datums[-1])
     log.write("\n")
     log.write("%s " % param)
     log.write("\n")


     flnr = 2
     while len(sys.argv) > flnr:
          filetoadd = sys.argv[flnr]
          datums2,values2,station2,param2 = read_dtb(filetoadd)
          log.write("%s " % 'add')
          log.write("%s " % station2)
          log.write("%s " % ':')
          log.write("%s " % datums2[0])
          log.write("%s " % 'till')
          log.write("%s " % datums2[-1])
          log.write("\n")
          log.write("%s " % param2)
          log.write("\n")

          print ( 'read', filetoadd,station2,param2)
          print ( ' merge',station,station2)
          params=match_params(param,param2)
          print (' matched',params)
          datums,values=merge(datums,values,datums2,values2,params)
          station = station2

          log.write("%s " % 'result')
          log.write("%s " % station)
          log.write("%s " % ':')
          log.write("%s " % datums[0])
          log.write("%s " % 'till')
          log.write("%s " % datums[-1])
          log.write("\n")

          print (' first',datums[0],datums2[0])
          print (' last',datums[-1],datums2[-1])
          print ('len',len(datums),len(datums2))
          flnr += 1
     if True : 
          npos = filetoadd.find('.csv')
          filexp=filetoadd[0:npos]+'_reeks.csv'
     print ( 'len check',len(params),len(datums),len(values))
     print (len(params),"params:",params)
     datums=numpy.sort(datums)
     OK=wrmerg(filexp,station,params,datums,values)
     print (OK)
     logexp = filexp.replace('.csv','.log')
     log.close()
     os.rename(logf,logexp)

exit
print('######################################################################')

