#!/bin/csh
#used by update_p13_data.sc!!!
#uses/needs merge.py and merge_one.py

set statnr = $1
set param = $2

if ( $param == '') then
echo " start with  :  Update_p13_data.sc statnr param "
exit
endif

# set normdir = /nobackup/users/scheele/Normalen/2020/Normalen_2020
# if  ($param == 'RD') set normdir = /nobackup/users/scheele/Normalen/2020/neerslag/Normalen_2020
# #if  ($statnr == '252_H') set normdir = /nobackup/users/scheele/Normalen/2020/Epis_2020
# if  ($statnr == 'NL') set normdir = Epis_2020

set art = 'gemiddeld'
if ($param == 'RH24') set art = 'som'
if ($param == 'EV24') set art = 'som'
if ($param == 'Q24') set  art = 'som'
if ($param == 'SQ24') set  art = 'som'
if ($param == 'RD') set  art = 'som'


set year = `date +%Y`
@ nyr = $year - 1

set name = ${statnr}_${param}

if ( $statnr == 'NL') goto noret
#goto skip
#goto join

### Retrieve daily data from ROBUKIS
cat ${param}_dayly.query | sed s^XXX_H^$statnr^ > retrieve_${name}.in 
        if (! -e tabel_${name}.csv) then
                wget --header=Content-Type:application/x-www-form-urlencoded --post-file=retrieve_${name}.in -O tabel_${name}.csv http://kisapp.knmi.nl:8080/servlet/download/table/19010101_000001/${nyr}1231_240000/CSV
        endif
        if ($statnr == '25_N' || $statnr == '770_N') then          
                echo "maak een gekoppelde reeks "
                if ($statnr == '25_N') set statnrx='9_N'
                if ($statnr == '770_N') set statnrx='745_N'
                cat ${param}_dayly.query | sed s^XXX_H^$statnrx^ > retr_reeks_${statnrx}.in 
                wget --header=Content-Type:application/x-www-form-urlencoded --post-file=retr_reeks_${statnrx}.in -O tabel_${statnrx}.csv http://kisapp.knmi.nl:8080/servlet/download/table/19010101_000001/${nyr}1231_240000/CSV                                               
                python merge.py tabel_${statnrx}.csv tabel_${name}.csv csv
                /bin/mv -f tabel_${statnrx}_reeks.csv tabel_${name}.csv
                rm retr_reeks_${statnrx}.in
                rm tabel_${statnrx}.csv
                rm tabel_${statnrx}_daily.csv
        endif
        if ( ! -e tabel_${name}.csv) then
                echo " Geen data gevonden $name $param "
                exit
        endif
        endif

### add yesterdays data
set today  = `date +%Y%m%d`

set janfirst=${year}'0101'
set mm = `date +%m`
if ($mm == '01' ) set janfirst=${nyr}'0101'

wget --header=Content-Type:application/x-www-form-urlencoded --post-file=retrieve_${name}.in -O tabel_${name}_aanv.csv http://kisapp.knmi.nl:8080/servlet/download/table/${janfirst}_000001/${today}_240000/CSV

rm retrieve_${name}.in

join:
### Make files for Rscript and keep data complete
python merge_one.py tabel_${name}.csv tabel_${name}_aanv.csv 
/bin/mv -f tabel_${name}_reeks.csv tabel_${name}.csv
                
