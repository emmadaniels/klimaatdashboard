#!/usr/bin/env python3
#used by update_p13_data.sc!!!
  
# import the needed packages
backend = 'Agg'

import os, sys,numpy,datetime
import csv


######################################################################
def wr_run(yest):
     yest = datums2[-1]
     yesterdat = yest[0:4] + '-'   + yest [4:6] + '-' + yest[6:8]
     yestwr=open('yesterday','w+')
     yestwr.write("%s" %  yesterdat)
     yestwr.close()
     currday=dayshift(yest,1)
     
     currdat = currday[0:4] + '-'   + currday [4:6] + '-' + currday[6:8]
     currwr=open('runday','w+')
     currwr.write("%s" %  currdat)
     currwr.close()
     return 'OK'
     
######################################################################
def dayshift(dtgb,fp):
     TP= datetime.date(int(dtgb[0:4]),int(dtgb[4:6]),int(dtgb[6:8]))
     TP2 = TP + datetime.timedelta(days=fp)
     dtg= datetime.datetime.strftime(TP2,'%Y%m%d')
     return str(dtg)

######################################################################
def read_dtb(filedtb):
    values={}
    nfound = 0 
    mparam = []
    datlist = []
    lnr=0
    if not os.path.exists(filedtb):
          print (' no filedtb : ',filedtb)
          sys.exit()
    with open(filedtb) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
          
          if lnr == 0:
              for jpar in range (len(row)):
                     cont = row[jpar].split('.')
                     try:
                          param = cont[1]
                     except:
                          param = cont[0]
                     mparam.append(param)
              print (' found :',mparam)
               
          else:
                station=row[1]
                datetime=row[0]
                datum=datetime[0:8]
                for jpar,param in enumerate(mparam):
                     try:
                        val=float(row[jpar+2])
                     except:
                        val=numpy.nan
                        
                     values[param,datum]=val
                     nfound +=1
                datlist.append(datum)
          lnr+=1
        if nfound == 0 :
             print (' no data found',dtgbeg,dtgend)
             sys.exit()

    return datlist, values,station,mparam

######################################################################
def read_forecast(filefc,colnr,params):
     values={}
     datums=[]
     jline = 0
     if not (os.path.exists(filefc)):
          print ( ' no file ',filefc)
          sys.exit()

     txtf=open(filefc,'rt') 
     while True:
          try:
              line = txtf.readline()
              row= line.split()
          except:
               print (' EOF')
               break
          if len(row) < 1 : break
          jline +=1
          if jline == 1 :
                dtan = row[0]
                station = row[1]
                print ( ' station',station,' analysis',dtan)
          else:
               datum=row[0]
               val=float(row[colnr])
               day = datum.replace('-','')

               datums.append(day)
               values[params[0],day]=val
               values[params[1],day]=station
               values[params[-2],day]=val
               values[params[-1],day]=numpy.nan

     txtf.close()
     return datums,values
######################################################################
def read_txt(filetxt,inddtg,param1,indval1,param2,indval2):
     values={}
     datums=()
     paramlist=[]
     station = 'unk'
     if not (os.path.exists(filetxt)):
          print ( ' no file ',filetxt)
          sys.exit()

     txtf=open(filetxt,'rt') 
     while True:
          try:
              line = txtf.readline()
              row= line.split()
          except:
               print (' EOF')
               break
          datum=row[inddtg][0:8]
          val1=row[indval1]
          val2=row[indval2]

          datums.append(datum)
          values[param1,datum]=val1
          values[param2,datum]=val2

     txtf.close()
     return datums,values,station,paramlist
######################################################################

def  match_params(param1,param2):

     parammatch=[]
     for jp1 in range(len(param1)):
          for jp2 in range(len(param2)):
               if param1[jp1] == param2[jp2] : 
                         parammatch.append(param1[jp1])
     return parammatch
######################################################################


def wrmerg(filexp,station,params,datums,values):
     csvf=open(filexp,'w+')
     nnan = 0 
     print ( station,' wrmerg params',params)
     for jpar in range(3):
           csvf.write("%s" % params[jpar].strip())
           csvf.write("%s" %  ',')
#     csvf.write("%s" % 't')
#     csvf.write("%s" % ',')
#     csvf.write("%s" % 'y')
     csvf.write('\n')
     nextdat = datums[0]
     for jdat in range(len(datums)):
          datum=datums[jdat]
          if datum != nextdat: print(' gapp',datum,nextdat)
          csvf.write("%s" %  datum)
          csvf.write("%s" %  ',')
          csvf.write("%s" %  station)
          csvf.write("%s" %  ',')
          if numpy.isnan(values[params[0],datums[jdat]]) : 
                    nnan +=1
                    print ('alarm: missing',datum,station,values[params[0],datums[jdat]])
          else:
                     csvf.write("%s" % values[params[0],datums[jdat]])
          csvf.write('\n')
          nextdat = dayshift(datum,1)
     csvf.close()
     return filexp

######################################################################
def wrdefdat(filexp,station,params,datums,values):
     csvf=open(filexp,'w+')
     csvf.write("%s" % station)
     if params[2] == 'TG' : csvf.write("%s" % ' Gemiddelde temperatuur ')
     if params[2] == 'TX' : csvf.write("%s" % ' Maximum temperatuur ')
     if params[2] == 'TN' : csvf.write("%s" % ' Minimum temperatuur ')
     csvf.write('\n')
     nnan = 0 
     csvf.write("%s" % 'day')
     csvf.write("%s" % ',')
     csvf.write("%s" % params[2])
     csvf.write('\n')
     for jdat in range(len(datums)):
          datum=datums[jdat]
          datum = datum[0:4] + '-'   + datum [4:6] + '-' + datum[6:8]
          csvf.write("%s" %  datum)
          csvf.write("%s" %  ',')
          if numpy.isnan(values[params[0],datums[jdat]]) : 
                    nnan +=1
          else:
                     csvf.write("%s" % values[params[0],datums[jdat]])
          csvf.write('\n')
     csvf.close()
     return filexp
######################################################################
def wrmonth(filexp,station,params,datums,values,month):
     csvf=open(filexp,'w+')
     nnan = 0 
#      fit for Rscrits
#           csvf.write("%s" % params[jpar].strip())
     csvf.write("%s" % 't')
     csvf.write("%s" % ',')
     csvf.write("%s" % 'y')
     csvf.write('\n')
     for jdat in range(len(datums)):
          datum=datums[jdat]
          mm=datum [4:6]
          datum = datum[0:4] + '-'   + datum [4:6] + '-' + datum[6:8]
          if str(mm) == str(month) : 
               csvf.write("%s" %  datum)
               csvf.write("%s" %  ',')
               if numpy.isnan(values[params[0],datums[jdat]]) : 
                    nnan +=1
               else:
                     csvf.write("%s" % values[params[0],datums[jdat]])
               csvf.write('\n')
     csvf.close()
     return filexp
######################################################################

def merge(datums1,values1,datums2,values2,params):
     repl,upd=0,0
     values=values1.copy()
     datums = datums1[:]
     print ( len (values))
     for datum in datums2:
          for param in params:
                 upd +=1
                 values[param,datum]=values2[param,datum]
                 if datum > datums[-1] :  datums.append(datum)
     return datums,values
######################################################################


### main
print('-----------------------------------------------------------')
print (' Merge_one.py  copies paramweter from 2 files to one, overwriting values of file1 for same date')
print (' Further adds forecast ')
print ('generates yesterday and runday, yesterday being last day with observation')
print ('generates extra file with corrected dtg format( yyyy-mm-dd) ')
if len(sys.argv) < 2:     
     print (' start : Merge_one.py tabel last_year forecast')
     print (' filtype : csv  csv txt ')
     print (' Not csv :  add posdtg,param1,pos1,param2,pos2')
     print (' pos: position in column, starting at 0')
else:
     filedtb = sys.argv[1]
     npos = filedtb.find('.csv')
     L_add = False
     if len(sys.argv) > 2:
          filetoadd = sys.argv[2]
          L_add = True
     L_FC=False
     if len(sys.argv) > 3 :
          filefc =  sys.argv[3]
          L_FC=True

     datums1,values1,station1,param1 = read_dtb(filedtb)
     print ( 'read', filedtb,station1,param1)
     if not L_add:
             fileR=filedtb[0:npos]+'_daily.csv'
             OK=wrdefdat(fileR,station1,param1,datums1,values1)
             print(OK)
             sys.exit()
     datums2,values2,station2,param2 = read_dtb(filetoadd)
     print ( 'read', filetoadd,station2,param2)
     print ( ' merge',station1,station2)
     params=match_params(param1,param2)
     print (' matched',params)
     datums,values=merge(datums1,values1,datums2,values2,params)
     print (' first',datums1[0],datums2[0],datums[0])
     print (' last',datums1[-1],datums2[-1],datums[-1])
     print ('len',len(datums1),len(datums2),len(datums))
     station = station2
     if station == 'unk' : station=station1
     filePY=filedtb[0:npos]+'_reeks.csv'
     fileR=filedtb[0:npos]+'_daily.csv'
     print ( 'len check',len(params),len(datums),len(values))
     print (len(params),"params:",params)
     OK=wrmerg(filePY,station,params,datums,values)
     print (OK)
     if L_FC:
          if params[2] == 'TN':
               days,valfp = read_forecast(filefc,1,params)
          elif  params[2] == 'TX':
               days,valfp = read_forecast(filefc,2,params)
          else:
               days,valfp = read_forecast(filefc,3,params)
          datums,values=merge(datums,values,days,valfp,params)
     OK=wrdefdat(fileR,station,params,datums,values)
     print (OK)

     OK=wr_run(datums2[-1])
print('######################################################################')

