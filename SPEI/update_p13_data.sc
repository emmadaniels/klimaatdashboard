#!/bin/csh
#can be run from (base) environment

#(Master)script to retrieve data from RobuKIS and merge up to yesterday and average for NL
#Needs files/scripts:
#-retrieve_parameter.sc that uses/needs merge.py and merge_one.py and ${param}_dayly.query in our case:
#EV24_dayly.query and RD_dayly.query
#-average_one.py

### retrieve data voor de P13 neerslagstations
foreach param (RD)
foreach statnr (11_N 25_N 139_N 144_N 222_N 328_N 438_N 550_N 666_N 737_N 770_N 828_N 961_N)
set name = $statnr
set year = `date +%Y`
@ nyr = $year - 1
csh  retrieve_parameter.sc  $statnr $param
end
### middel de data
python average_one.py  tabel_11_N_${param}.csv tabel_25_N_${param}.csv tabel_139_N_${param}.csv tabel_144_N_${param}.csv tabel_222_N_${param}.csv tabel_328_N_${param}.csv tabel_438_N_${param}.csv tabel_550_N_${param}.csv tabel_666_N_${param}.csv tabel_737_N_${param}.csv tabel_770_N_${param}.csv tabel_828_N_${param}.csv tabel_961_N_${param}.csv
### eventueel kun je de tend en de schatting van het lopende jaar uitrekenen. Geeft een hoop extrra files.
#csh  Update_parameter_trend.sc NL $param
end


### retrieve de P13 bijbehorende verdampuing van de AWS stations
foreach param (EV24)
foreach statnr (251_H 235_H 280_H 286_H 249_H 278_H 240_H 260_H 283_H 310_H 319_H 370_H 377_H)
csh  retrieve_parameter.sc  $statnr $param
end
### middel de data
python average_one.py  tabel_251_H_${param}.csv tabel_235_H_${param}.csv tabel_280_H_${param}.csv tabel_286_H_${param}.csv tabel_249_H_${param}.csv tabel_278_H_${param}.csv tabel_240_H_${param}.csv tabel_260_H_${param}.csv tabel_283_H_${param}.csv tabel_310_H_${param}.csv tabel_319_H_${param}.csv tabel_370_H_${param}.csv tabel_377_H_${param}.csv
### eventueel kun je de tend en de schatting van het lopende jaar uitrekenen. Geeft een hoop extrra files.
#csh  Update_parameter_trend.sc NL param
end

/bin/mv tabel_* ./DATA
exit

# Zo zijn de neerslagstations aan AWS verdamping gekoppeld. 

#   P13   AWS   NL
#  11_N  251_H Hoorn Terschelling
#  25_N 235_H  De Kooy @
# 139_N 280_H  Eelde @
# 144_N 286_H  Nieuw Beerta
# 222_N 249_H  Berkhout
# 328_N 278_H  Heino
# 438_N 240_H  Schiphol
# 550_N 260_H  De Bilt @
# 666_N 283_H  Hupsel
# 737_N 310_H  Vlissingen @
# 770_N 319_H  Westdorpe
# 828_N 370_H  Eindhoven
# 961_N 377_H  Ell 

# @)  LH05 station

# Opm: 380_H Maastricht zit wel in LH05, maar niet in P13
